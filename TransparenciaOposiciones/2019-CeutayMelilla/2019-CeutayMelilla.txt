Enunciados de la convocatoria extraordinaria de Ceuta y Melilla de 2019 para FQ

Respeco a Melilla, le adjuntamos 3 archivos Word con el examen de Física y Química realizado en 2019. Dichos archivos corresponden al Cuestionario de Física y Química, el Cuestionario de Química y al Examen práctico.
Los cuestionarios constaban de 25 preguntas, cada una de ellas con 4 respuestas alternativas.
En el examen práctico había que elegir dos ejercicios, uno de Física y otro de Química, cada uno de los cuales tenía dos preguntas.

Respecto a Ceuta, le adjuntamos archivo pdf con el examen de Física y Química realizado en 2019. Dicho examen constaba de 4 ejercicios con varias preguntas cada uno de ellos.
La puntuación de cada pregunta era de 2,5 puntos.
Era necesario lograr 5 puntos para obtener la calificación de apto.

No envían soluciones, esos 4 ficheros es lo que dicen que tienen.
